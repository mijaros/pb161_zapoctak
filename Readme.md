# Internet of Things

## Představení úkolu
IoT jako termín označuje síť vestavěných (embedded) zařizení, která spolu komunikují pomocí existující síťové infrastruktury. Toto umožňuje sbírání dat, či řízení těchto zařízení.
Pro jednoduchost si můžete představit síť senzorů a mikro ovladačů, které nějakým způsobem interagují (například teplotní senzor vyšle signál servo motůrku, který uzavře ventil topení v místnosti)

## Zadání
V našem příkladu budou vystupovat tři klíčové elementy

* _IO zařízení_ reprezentuje jeden aktivní prvek, či sensor
* _Mikrozařízení_ (obsluhuje Io zařízení)
* _Gateway_ (kontrolní bod sítě, který sbírá data a rozhoduje akce)

Vaším úkolem bude implementovat vhodnou objektovou hiearchii pro tento příklad se splněním následujících podmínek a přidat jednoduché běhové rozhraní, na kterém demonstrujete vaši implementaci.

### Podmínky
__IO Zařízení:__

* Drží informaci zda je Input či Output zařízením
* Do každé zařízení má metodu pro přečtení a metotu pro zápis hodnot -- uvažujeme celá čísla.
* Drží informaci o objemu spotřebované energie - cele číslo v mWh (mikro watt hodiny)
* Lze z něj získat hodnout, či ji přečíst
* U výstupních zařízení indikuje získání hodnoty, zda je zařízení zapnuté či vypnuté
* U vstupních zařízení nedělá zápis hodnoty nic
* Dále implementujte následující zařízení
    * Teploměr -- vsupní zařízení, se spotřebou 5mWH, vrací hodnotu mezi -10 -- 60
    * Daylight sensor -- vstupní zařízení se spotřebou 15mWH, vrací hodnout mezi 0 - 100
    * Led Dioda -- výstupní zařízení, spotřeba 10mWH pokud je zapnutá 0 jinak, zapnutá je jakoukoliv nenulovou hodnotou 0 vypíná, vrací 1 nebo 0
    * Servo motor -- výstupní zařízení, vstup mezi 0 -- 100 značí aktuální výkon motoru, spotřeba je ```(aktuální výkon * 20)/100```, vrací aktuální výkon 
    

__Mikrozařízení:__

* Každé mikrozařízení má několik sběrnic (Pin), na kterých může být připojeno Io zařízení
* Každé mikrozařízení má jednoznačný identifikátor
* Každému mikrozařízení lze připojovat a odpojovat aktivní prvky
* Každá deska má metodu, která vykonává tik procesoru, tato metoda kontroluje, zda nebyla překročena celková spotřeba a zasílá gateway informaci o stavu jednodlivých připojených zařízení
* Dále budete implementovat jedno z následujících mikrozařízení, která se liší v přístupu k Pinům
* __RaspberyPi__:
    * Piny jsou označeny celým číslem, které odpovídá posunu v paměti
    * Pro jednoduchost nemusíte uvažovat žádný offset a indexovat od nuly
    * Raspberry Pi ma 20 pinu
    * Raspberry Pi dokáže napájet zařízení o celkovém výkonu 60 mWH
* __CubieBoard__:
    * Piny jsou označeny řetězcem, dle jejich připojení v SysFS
    * CubieBoard obsahuje 10 pinů -- jejich názvy (stringy) jsou čistě na vás
    * CubieBoard dokáže obsloužit 100 mWH výkonu na periferiích
* __Můžete si vybrat pouze jedno z výše uvedených zařízení, implementace obou bude brána jako bonus__
    
__Gateway:__

* Sbírá ze zařízení informace o jejich stavu
* Všem zařízením zasílá informaci zda mají zapnout, či vypnout své aktivní prvky (zároveň s eventuální úrovní výkonu)
* Vyvolává na jednotlivých zařízeních tik procesoru
* Ukládá si předaný stav mikrozařízení do vhodného kontejneru.

### Požadavky
* Vhodně dekomponujte problém a vytvořte co možná nejlepší objektovou hiearchii
* Použijte vhodné STL kontejnery
* Použijte vhodně vyhazování a ošetřování výjimek
* V běhovém prostředí demonstrujte situace běhu jednotlivých zařízení, stejně jako situaci přetížení konkrétního zařízení.
